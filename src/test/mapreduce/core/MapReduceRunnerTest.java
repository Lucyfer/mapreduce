package mapreduce.core;

import junit.framework.Assert;
import mapreduce.core.mapper.IMapper;
import mapreduce.core.mapper.IdentityMapper;
import mapreduce.core.reducer.IReducer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

public class MapReduceRunnerTest {
    private final IMapper<String, String, String, String> mapper = new IdentityMapper<>();
    private final IReducer<String, String> reducer = new IReducer<String, String>() {
        @Override
        public String reduce(KVPair<String, Iterable<String>> kvPair) {
            StringBuilder sb = new StringBuilder();
            for (String value : kvPair.value) {
                sb.append(value);
            }
            return sb.toString();
        }
    };

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testMapReduce_identity_test() throws Exception {
        final MapReduceRunner<String, String, String, String> mapReduceRunner = new MapReduceRunner<>(mapper, reducer);

        ArrayList<KVPair<String, String>> input = new ArrayList<>();
        input.add(new KVPair<>("a", "c"));
        input.add(new KVPair<>("b", "d"));
        input.add(new KVPair<>("b", "d"));
        input.add(new KVPair<>("a", "c"));

        HashMap<String, String> expected = new HashMap<>(2);
        expected.put("a", "cc");
        expected.put("b", "dd");

        final Iterable<KVPair<String, String>> actual = mapReduceRunner.mapReduce(input);

        final Iterator<KVPair<String, String>> actualIterator = actual.iterator();
        int i = 0;
        for (; actualIterator.hasNext(); ++i) {
            KVPair<String, String> kv = actualIterator.next();
            Assert.assertEquals(expected.get(kv.key), kv.value);
        }
        Assert.assertEquals(expected.size(), i);
    }

    @Test
    public void testMapReduce_word_count() throws Exception {
        final IMapper<String, String, String, Integer> mapper = new IMapper<String, String, String, Integer>() {
            @Override
            public Iterable<KVPair<String, Integer>> map(KVPair<String, String> kvPair) {
                final String[] strings = kvPair.value.split(" ");
                return new Iterable<KVPair<String, Integer>>() {
                    @Override
                    public Iterator<KVPair<String, Integer>> iterator() {
                        return new Iterator<KVPair<String, Integer>>() {
                            private int i = 0;

                            @Override
                            public boolean hasNext() {
                                return i < strings.length;
                            }

                            @Override
                            public KVPair<String, Integer> next() {
                                return new KVPair<>(strings[i++], 1);
                            }

                            @Override
                            public void remove() {
                                throw new UnsupportedOperationException();
                            }
                        };
                    }
                };
            }
        };
        final IReducer<String, Integer> reducer = new IReducer<String, Integer>() {
            @Override
            public Integer reduce(KVPair<String, Iterable<Integer>> kvPair) {
                int count = 0;
                for (int usages : kvPair.value) {
                    count += usages;
                }
                return count;
            }
        };
        final MapReduceRunner<String, String, String, Integer> mapReduceRunner = new MapReduceRunner<>(mapper, reducer);
        final ArrayList<KVPair<String, String>> input = new ArrayList<>();
        input.add(new KVPair<>("file1", "The quick brown fox jumps over the lazy dog"));
        input.add(new KVPair<>("file2", "Two driven jocks help fax my big quiz"));
        input.add(new KVPair<>("file3", "Jackdaws love my big sphinx of quartz"));

        final int expectedCount = 22;
        final HashMap<String, Integer> expected = new HashMap<>();
        expected.put("The", 1);
        expected.put("my", 2);
        expected.put("big", 2);
        expected.put("sphinx", 1);
        expected.put("jocks", 1);
        expected.put("of", 1);
        expected.put("Jackdaws", 1);

        Iterable<KVPair<String, Integer>> pairs = mapReduceRunner.mapReduce(input);

        int count = 0;
        for (KVPair<String, Integer> pair : pairs) {
            if (expected.containsKey(pair.key)) {
                Assert.assertEquals(pair.key, expected.get(pair.key), pair.value);
            }
            count++;
        }
        Assert.assertEquals(expectedCount, count);
    }

    @Test
    public void testGroupIterator() throws Exception {
        final MapReduceRunner<String, String, String, String> mapReduceRunner = new MapReduceRunner<>(mapper, reducer);
        final ArrayList<KVPair<String, Iterable<String>>> expected = new ArrayList<>(4);
        // pairs are expected in the sorted order
        expected.add(new KVPair<String, Iterable<String>>("a", Arrays.asList("1", "5")));
        expected.add(new KVPair<String, Iterable<String>>("b", Arrays.asList("2", "6")));
        expected.add(new KVPair<String, Iterable<String>>("c", Arrays.asList("3", "7")));
        expected.add(new KVPair<String, Iterable<String>>("d", Arrays.asList("4", "8")));

        final ArrayList<KVPair<String, String>> input = new ArrayList<>();
        input.add(new KVPair<>("a", "1"));
        input.add(new KVPair<>("b", "2"));
        input.add(new KVPair<>("c", "3"));
        input.add(new KVPair<>("d", "4"));
        input.add(new KVPair<>("d", "8"));
        input.add(new KVPair<>("c", "7"));
        input.add(new KVPair<>("b", "6"));
        input.add(new KVPair<>("a", "5"));

        MapReduceRunner.GroupIterator groupIterator = mapReduceRunner.new GroupIterator(input);

        @SuppressWarnings("unchecked")
        Iterator<KVPair<String, Iterable<String>>> iterator = groupIterator.iterator();
        int i = 0;
        for (; iterator.hasNext(); ++i) {
            KVPair<String, Iterable<String>> kv = iterator.next();
            Assert.assertEquals(expected.get(i), kv);
        }
        Assert.assertEquals(expected.size(), i);
    }
}
