package mapreduce.core.grouper;

import org.junit.Test;
import mapreduce.core.KVPair;

import java.util.*;

import static junit.framework.Assert.*;

public class DefaultGrouperTest {

    @Test
    public void testGroup_trivial() throws Exception {
        final DefaultGrouper<String, Integer> grouper = new DefaultGrouper<>();

        final ArrayList<KVPair<String, Integer>> pairs = new ArrayList<>();
        pairs.add(new KVPair<>("a", 1));
        pairs.add(new KVPair<>("a", 2));
        pairs.add(new KVPair<>("a", 3));
        pairs.add(new KVPair<>("a", 4));

        final List<Integer> expectedValues = Arrays.asList(1, 2, 3, 4);

        Iterable<KVPair<String, Iterable<Integer>>> actual = grouper.group(pairs);
        Iterator<KVPair<String, Iterable<Integer>>> actualIterator = actual.iterator();
        KVPair<String, Iterable<Integer>> actualPair = actualIterator.next();

        assertEquals("a", actualPair.key);
        assertEquals(expectedValues, actualPair.value);
        assertFalse(actualIterator.hasNext());
    }

    @Test
    public void testGroup_complex() throws Exception {
        final DefaultGrouper<String, Integer> grouper = new DefaultGrouper<>();

        final ArrayList<KVPair<String, Integer>> pairs = new ArrayList<>();
        pairs.add(new KVPair<>("a", 1));
        pairs.add(new KVPair<>("b", 2));
        pairs.add(new KVPair<>("c", 3));
        pairs.add(new KVPair<>("d", 4));

        pairs.add(new KVPair<>("a", 5));
        pairs.add(new KVPair<>("b", 6));
        pairs.add(new KVPair<>("c", 7));
        pairs.add(new KVPair<>("d", 8));

        final HashMap<String, List<Integer>> expectedValues = new HashMap<>(4);
        expectedValues.put("a", Arrays.asList(1, 5));
        expectedValues.put("b", Arrays.asList(2, 6));
        expectedValues.put("c", Arrays.asList(3, 7));
        expectedValues.put("d", Arrays.asList(4, 8));

        final Iterable<KVPair<String, Iterable<Integer>>> actual = grouper.group(pairs);
        final Iterator<KVPair<String, Iterable<Integer>>> actualIterator = actual.iterator();

        for (int i = 0; i < 4; ++i) {
            KVPair<String, Iterable<Integer>> actualPair = actualIterator.next();
            assertTrue(expectedValues.containsKey(actualPair.key));
            assertEquals(expectedValues.get(actualPair.key), actualPair.value);
        }

        assertFalse(actualIterator.hasNext());
    }
}
