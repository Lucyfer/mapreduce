package mapreduce.core;

import mapreduce.core.grouper.DefaultGrouper;
import mapreduce.core.grouper.IGrouper;
import mapreduce.core.mapper.IMapper;
import mapreduce.core.reducer.IReducer;

import java.util.Iterator;

public class MapReduceRunner<K1, V1, K2, V2> {

    protected final IMapper<K1, V1, K2, V2> mapper;
    protected final IReducer<K2, V2> reducer;
    protected final IGrouper<K2, V2> grouper;
    protected final EmptyIterator<KVPair<K2, V2>> EMPTY_ITERATOR = new EmptyIterator<>();

    public MapReduceRunner(IMapper<K1, V1, K2, V2> mapper, IReducer<K2, V2> reducer, IGrouper<K2, V2> grouper) {
        this.grouper = grouper;
        this.mapper = mapper;
        this.reducer = reducer;
    }

    public MapReduceRunner(IMapper<K1, V1, K2, V2> mapper, IReducer<K2, V2> reducer) {
        this(mapper, reducer, new DefaultGrouper<K2, V2>());
    }

    Iterable<KVPair<K2, V2>> mapReduce(Iterable<KVPair<K1, V1>> input) {
        final MapIterator mapIterator = new MapIterator(input);
        final GroupIterator groupIterator = new GroupIterator(mapIterator);
        return new ReduceIterator(groupIterator);
    }

    protected final class MapIterator implements Iterable<KVPair<K2, V2>> {

        private final Iterator<KVPair<K1, V1>> input;
        private Iterator<KVPair<K2, V2>> mapIterator;

        public MapIterator(Iterable<KVPair<K1, V1>> input) {
            this.input = input.iterator();
            this.mapIterator = EMPTY_ITERATOR;
        }

        @Override
        public Iterator<KVPair<K2, V2>> iterator() {
            return new Iterator<KVPair<K2, V2>>() {
                @Override
                public boolean hasNext() {
                    if (!mapIterator.hasNext()) {
                        mapIterator = input.hasNext() ? mapper.map(input.next()).iterator() : EMPTY_ITERATOR;
                    }
                    return mapIterator.hasNext();
                }

                @Override
                public KVPair<K2, V2> next() {
                    return mapIterator.next();
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException("Cannot remove MapReduce input items");
                }
            };
        }
    }

    protected final class GroupIterator implements Iterable<KVPair<K2, Iterable<V2>>> {

        private final Iterator<KVPair<K2, Iterable<V2>>> innerGroupIterator;

        public GroupIterator(Iterable<KVPair<K2, V2>> mapIterator) {
            this.innerGroupIterator = grouper.group(mapIterator).iterator();
        }

        @Override
        public Iterator<KVPair<K2, Iterable<V2>>> iterator() {
            return new Iterator<KVPair<K2, Iterable<V2>>>() {
                @Override
                public boolean hasNext() {
                    return innerGroupIterator.hasNext();
                }

                @Override
                public KVPair<K2, Iterable<V2>> next() {
                    return innerGroupIterator.next();
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException("Cannot remove MapReduce grouped items");
                }
            };
        }
    }

    protected final class ReduceIterator implements Iterable<KVPair<K2, V2>> {
        private final Iterator<KVPair<K2, Iterable<V2>>> groups;

        public ReduceIterator(Iterable<KVPair<K2, Iterable<V2>>> groups) {
            this.groups = groups.iterator();
        }

        @Override
        public Iterator<KVPair<K2, V2>> iterator() {
            return new Iterator<KVPair<K2, V2>>() {
                @Override
                public boolean hasNext() {
                    return groups.hasNext();
                }

                @Override
                public KVPair<K2, V2> next() {
                    final KVPair<K2, Iterable<V2>> grouped = groups.next();
                    return new KVPair<>(grouped.key, reducer.reduce(grouped));
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException("Cannot remove MapReduce reduced items");
                }
            };
        }
    }

    protected static final class EmptyIterator<T> implements Iterator<T> {
        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public T next() {
            return null;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Cannot remove items from Empty Iterator");
        }
    }
}
