package mapreduce.core.mapper;

import mapreduce.core.KVPair;

import java.util.ArrayList;

public class IdentityMapper<K1, V1, K2, V2> implements IMapper<K1, V1, K1, V1> {
    @Override
    public Iterable<KVPair<K1, V1>> map(KVPair<K1, V1> kvPair) {
        ArrayList<KVPair<K1, V1>> result = new ArrayList<>(1);
        result.add(kvPair);
        return result;
    }
}
