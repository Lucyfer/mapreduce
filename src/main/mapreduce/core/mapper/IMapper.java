package mapreduce.core.mapper;

import mapreduce.core.KVPair;

public interface IMapper<K1, V1, K2, V2> {
    Iterable<KVPair<K2, V2>> map(KVPair<K1, V1> kvPair);
}
