package mapreduce.core.grouper;

import mapreduce.core.KVPair;

public interface IGrouper<K2, V2> {
    Iterable<KVPair<K2, Iterable<V2>>> group(Iterable<KVPair<K2, V2>> pairs);
}
