package mapreduce.core.grouper;

import mapreduce.core.KVPair;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class DefaultGrouper<K2, V2> implements IGrouper<K2, V2> {
    @Override
    public Iterable<KVPair<K2, Iterable<V2>>> group(final Iterable<KVPair<K2, V2>> pairs) {
        final TreeMap<K2, ArrayList<V2>> groups = new TreeMap<>();
        for (KVPair<K2, V2> pair : pairs) {
            if (groups.containsKey(pair.key)) {
                groups.get(pair.key).add(pair.value);
            } else {
                ArrayList<V2> v = new ArrayList<>();
                v.add(pair.value);
                groups.put(pair.key, v);
            }
        }

        final ArrayList<KVPair<K2, Iterable<V2>>> result = new ArrayList<>(groups.size());
        for (Map.Entry<K2, ArrayList<V2>> entry : groups.entrySet()) {
            result.add(new KVPair<K2, Iterable<V2>>(entry.getKey(), entry.getValue()));
        }

        return result;
    }
}
