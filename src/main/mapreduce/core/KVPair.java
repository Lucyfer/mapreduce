package mapreduce.core;

public class KVPair<K, V> {
    public final K key;
    public final V value;

    public KVPair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KVPair kvPair = (KVPair) o;
        return kvPair.key.equals(key) && kvPair.value.equals(value);
    }

    @Override
    public String toString() {
        return "KVPair(" + key + ", " + value + ')';
    }
}
