package mapreduce.core.reducer;

import mapreduce.core.KVPair;

public interface IReducer<K2, V2> {
    V2 reduce(KVPair<K2, Iterable<V2>> kvPair);
}
